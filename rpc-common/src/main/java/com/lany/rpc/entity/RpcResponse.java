package com.lany.rpc.entity;

import com.lany.rpc.enumeration.ResponseCode;
import lombok.Data;

import java.io.Serializable;

/**
 * @author liuyanyan
 * @date 2021/12/21 14:57
 * 服务调用成功或者失败返回的响应信息
 */
@Data
public class RpcResponse<T> implements Serializable {

    /**
     * 对应的请求id
     */
    private String requestID;

    /**
     * 响应状态码
     */
    private Integer statusCode;

    /**
     * 响应状态补充信息
     */
    private String message;

    /**
     * 响应数据
     */
    private T data;

    public RpcResponse() {
    }

    /**
     * 返回成功的相应数据
     *
     * @param data
     * @param <T>
     * @return
     */
    public static <T> RpcResponse<T> success(T data, String requestID) {
        RpcResponse<T> response = new RpcResponse<>();
        response.setRequestID(requestID);
        response.setStatusCode(ResponseCode.SUCCESS.getCode());
        response.setData(data);
        return response;
    }

    /**
     * 返回失败的响应信息
     *
     * @param code
     * @param <T>
     * @return
     */
    public static <T> RpcResponse<T> fail(ResponseCode code, String requestID) {
        RpcResponse<T> response = new RpcResponse<>();
        response.setRequestID(requestID);
        response.setStatusCode(code.getCode());
        response.setMessage(code.getMessage());
        return response;
    }

}
