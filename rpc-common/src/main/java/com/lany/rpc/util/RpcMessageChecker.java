package com.lany.rpc.util;

import com.lany.rpc.entity.RpcRequest;
import com.lany.rpc.entity.RpcResponse;
import com.lany.rpc.enumeration.ResponseCode;
import com.lany.rpc.enumeration.RpcError;
import com.lany.rpc.exception.RpcException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author liuyanyan
 * @date 2021/12/28 16:22
 * 检查响应与请求是否对应
 */
public class RpcMessageChecker {

    public static final Logger logger = LoggerFactory.getLogger(RpcMessageChecker.class);

    public static final String INTERFACE_NAME = "interfaceName";

    public RpcMessageChecker() {
    }

    public static void check(RpcRequest rpcRequest, RpcResponse rpcResponse) {
        if (rpcResponse == null) {
            logger.error("服务调用失败，service：{}", rpcRequest.getInterfaceName());
            throw new RpcException(RpcError.SERVICE_INVOCATION_FAILURE, INTERFACE_NAME + rpcRequest.getInterfaceName());
        }
        if (!rpcResponse.getRequestID().equals(rpcRequest.getRequestId())) {
            logger.error("响应与请求号不符,request: {}, response: {}", rpcRequest.getRequestId(), rpcResponse.getRequestID());
            throw new RpcException(RpcError.RESPONSE_NOT_MATCH);
        }
        if (rpcResponse.getStatusCode() == null || rpcResponse.getStatusCode() != ResponseCode.SUCCESS.getCode()) {
            logger.error("调用服务失败, service: {}, response:{}", rpcRequest.getInterfaceName(), rpcResponse);
            throw new RpcException(RpcError.SERVICE_INVOCATION_FAILURE, INTERFACE_NAME + rpcRequest.getInterfaceName());
        }
    }

}
