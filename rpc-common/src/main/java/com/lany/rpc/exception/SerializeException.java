package com.lany.rpc.exception;

/**
 * @author jinchen
 * @version 1.0
 * @date 2021/12/26 17:11
 */
public class SerializeException extends RuntimeException {
    public SerializeException(String name) {
        super(name);
    }
}
