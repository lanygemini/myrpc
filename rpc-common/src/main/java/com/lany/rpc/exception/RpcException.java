package com.lany.rpc.exception;

import com.lany.rpc.enumeration.RpcError;

/**
 * @author liuyanyan
 * @date 2021/12/22 14:02
 */
public class RpcException extends RuntimeException {

    public RpcException(RpcError error, String message) {
        super(error.getMessage() + ": " + message);
    }

    public RpcException(RpcError error) {
        super(error.getMessage());
    }

    public RpcException(String message, Throwable cause) {
        super(message, cause);
    }

}
