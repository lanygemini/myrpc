package com.lany.rpc.factory;

import com.alibaba.nacos.api.naming.pojo.Instance;

import java.util.HashMap;
import java.util.Map;

/**
 * @author liuyanyan
 * @date 2021/12/31 14:20
 */
public class SingletonFactory {
    private static Map<Class, Object> map = new HashMap<>();

    public SingletonFactory() {
    }

    public static <T> T getInstance(Class<T> clazz) {
        Object o = map.get(clazz);
        synchronized (clazz) {
            if (o == null) {
                try {
                    o = clazz.newInstance();
                    map.put(clazz, o);
                } catch (InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return clazz.cast(o);
    }
}
