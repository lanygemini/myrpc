package com.lany.rpc.factory;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import jdk.nashorn.internal.ir.Block;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.*;

/**
 * @author liuyanyan
 * @date 2021/12/29 15:51
 * 创建线程池的工具类
 */
public class ThreadPoolFactory {
    private static final Logger logger = LoggerFactory.getLogger(ThreadPoolFactory.class);
    // 核心线程数
    private static final int CORE_POOL_SIZE = 10;
    // 最大线程数
    private static final int MAXIMUM_POOL_SIZE = 100;
    // 空闲线程的等待时间
    private static final long KEEP_ALIVE_TIME = 1;
    // 阻塞队列的容量
    private static final int BLOCKING_QUEUE_CAPACITY = 100;

    private static Map<String, ExecutorService> threadPoolsMap = new ConcurrentHashMap<>();

    public ThreadPoolFactory() {
    }

    public static ExecutorService createDefaultThreadPool(String threadNamePrefix) {
        return createDefaultThreadPool(threadNamePrefix, false);
    }

    public static ExecutorService createDefaultThreadPool(String threadNamePrefix, Boolean daemon) {
        ExecutorService threadPool = threadPoolsMap.computeIfAbsent(threadNamePrefix, key -> createThreadPool(threadNamePrefix, daemon));
        if (threadPool.isShutdown() || threadPool.isTerminated()) {
            threadPoolsMap.remove(threadNamePrefix);
            threadPool = createThreadPool(threadNamePrefix, daemon);
            threadPoolsMap.put(threadNamePrefix, threadPool);
        }
        return threadPool;
    }

    public static void shutDownAll() {
        logger.info("关闭所有线程池...");
        threadPoolsMap.entrySet().parallelStream().forEach(entry -> {
            ExecutorService executorService = entry.getValue();
            executorService.shutdown();
            logger.info("关闭线程池[{}][{}]", entry.getKey(), executorService.isTerminated());
            try {
                executorService.awaitTermination(10, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                logger.error("关闭线程池失败！");
                executorService.shutdownNow();
            }
        });
    }

    private static ExecutorService createThreadPool(String threadNamePrefix, Boolean daemon) {
        // 使用有界队列
        ArrayBlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<>(BLOCKING_QUEUE_CAPACITY);
        ThreadFactory threadFactory = createThreadFactory(threadNamePrefix, daemon);
        return new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, KEEP_ALIVE_TIME, TimeUnit.SECONDS, workQueue, threadFactory);
    }

    /**
     * 创建线程工厂，如果threadNamePrefix不为空，就使用自定义的线程工厂，否则使用defaultThreadFactory
     *
     * @param threadNamePrefix 线程名称的前缀
     * @param daemon           是否设置为守护线程
     * @return
     */
    private static ThreadFactory createThreadFactory(String threadNamePrefix, Boolean daemon) {
        if (threadNamePrefix != null) {
            if (daemon != null) {
                return new ThreadFactoryBuilder().setNameFormat(threadNamePrefix + "-%d").setDaemon(daemon).build();
            } else {
                return new ThreadFactoryBuilder().setNameFormat(threadNamePrefix + "-%d").build();
            }
        }
        return Executors.defaultThreadFactory();
    }

}
