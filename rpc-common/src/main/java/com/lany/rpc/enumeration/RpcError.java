package com.lany.rpc.enumeration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

/**
 * @author liuyanyan
 * @date 2021/12/22 13:45
 * 调用过程中的初选的错误
 */
@AllArgsConstructor
@Getter
public enum RpcError {

    /**
     * 客户端连接服务端失败
     */
    CLIENT_CONNENT_SERVER_FAILURE("客户端连接服务端失败"),

    /**
     * 服务调用失败
     */
    SERVICE_INVOCATION_FAILURE("服务调用失败"),

    /**
     * 未发现服务
     */
    SERVICE_NOT_FOUND("未发现服务"),

    /**
     * 服务未实现接口
     */
    SERVICE_NOT_IMPLEMENT_ANY_INTERFACE("服务未实现任何接口"),

    /**
     * 不识别的协议包
     */
    UNKNOW_PROROCOL("不识别的协议包"),

    /**
     * 不识别的序列化器
     */
    UNKNOWN_SERIALIZER("不识别的（反）序列化器"),

    /**
     * 不识别的数据包类型
     */
    UNKNOWN_PACKAGE_TYPE("不识别的数据包类型"),

    /**
     * 未设置序列化器
     */
    SERIALIZER_NOT_FOUND("找不到序列化器"),

    /**
     * 响应与请求号不匹配
     */
    RESPONSE_NOT_MATCH("响应与请求号不匹配"),

    /**
     * 连接注册中心失败
     */
    FAILED_TO_CONNECT_TO_SERVICE_REGISTRY("连接注册中心失败"),

    /**
     * 注册服务失败
     */
    REGISTER_SERVICE_FAILED("注册服务失败"),

    /**
     * 获取服务失败
     */
    OBTAIN_SERVICE_FAILED("获取服务失败");


    private final String message;

}
