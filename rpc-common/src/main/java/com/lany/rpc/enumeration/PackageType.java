package com.lany.rpc.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author jinchen
 * @version 1.0
 * @date 2021/12/25 16:00
 */
@AllArgsConstructor
@Getter
public enum PackageType {

    REQUEST_PACK(0),
    RESPONSE_PACK(1);

    private final int code;

}
