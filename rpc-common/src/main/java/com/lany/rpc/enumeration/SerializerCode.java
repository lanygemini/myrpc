package com.lany.rpc.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author jinchen
 * @version 1.0
 * @date 2021/12/25 15:59
 * <p>
 * 字节流中标识序列化和反序列化器
 */
@AllArgsConstructor
@Getter
public enum SerializerCode {
    KRYO(0),
    JSON(1);
    private final int code;
}
