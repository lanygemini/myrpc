package com.lany.rpc.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author liuyanyan
 * @date 2021/12/21 15:03
 */
@AllArgsConstructor
@Getter
public enum ResponseCode {

    /**
     * 调用成功
     */
    SUCCESS(200, "调用方法成功"),

    /**
     * 调用方法失败
     */
    FAIL(500, "调用方法失败"),

    /**
     * 未找到制定方法
     */
    METHOD_NOT_FOUND(500, "未找到指定方法"),

    /**
     * 未找到指定类
     */
    CLASS_NOT_FOUND(500, "未找到指定类");

    private final int code;
    private final String message;

}
