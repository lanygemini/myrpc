package com.lany.test;

import com.lany.api.HelloObject;
import com.lany.api.HelloService;
import com.lany.rpc.RpcClientProxy;
import com.lany.rpc.serializer.CommonSerializer;
import com.lany.rpc.transport.netty.client.NettyClient;
import com.lany.rpc.serializer.KryoSerializer;

/**
 * @author jinchen
 * @version 1.0
 * @date 2021/12/25 17:11
 */
public class NettyTestClient {
    public static void main(String[] args) {
        NettyClient cli = new NettyClient();
        RpcClientProxy clientProxy = new RpcClientProxy(cli);
        HelloService helloService = clientProxy.getProxy(HelloService.class);
        String res = helloService.hello(new HelloObject(12, "this is a messge"));
        System.out.println(res);
    }
}