package com.lany.test;

import com.lany.api.HelloObject;
import com.lany.api.HelloService;
import com.lany.rpc.RpcClientProxy;
import com.lany.rpc.serializer.KryoSerializer;
import com.lany.rpc.transport.socket.client.SocketClient;

/**
 * @author liuyanyan
 * @date 2021/12/21 16:04
 */
public class SocketTestClient {
    public static void main(String[] args) {
        SocketClient socketClient = new SocketClient();
        RpcClientProxy rpcClientProxy = new RpcClientProxy(socketClient);
        HelloService helloService = rpcClientProxy.getProxy(HelloService.class);
        HelloObject helloObject = new HelloObject(12, "this is a message");
        String hello = helloService.hello(helloObject);
        System.out.println(hello);

    }

}
