package com.lany.rpc.transport.netty.client;

import com.lany.rpc.RpcClient;
import com.lany.rpc.entity.RpcRequest;
import com.lany.rpc.entity.RpcResponse;
import com.lany.rpc.enumeration.RpcError;
import com.lany.rpc.exception.RpcException;
import com.lany.rpc.loadbalance.LoadBalancer;
import com.lany.rpc.loadbalance.RandomLoadBalancer;
import com.lany.rpc.registry.NacosServiceDiscovery;
import com.lany.rpc.registry.NacosServiceRegistry;
import com.lany.rpc.registry.ServiceDiscovery;
import com.lany.rpc.registry.ServiceRegistry;
import com.lany.rpc.serializer.CommonSerializer;
import com.lany.rpc.util.RpcMessageChecker;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.AttributeKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.net.InetSocketAddress;
import java.util.EventListener;
import java.util.concurrent.atomic.AtomicReference;


/**
 * @author jinchen
 * @version 1.0
 * @date 2021/12/25 15:05
 * <p>
 * 基于Netty的客户端连接
 */
public class NettyClient implements RpcClient {
    private static final Logger logger = LoggerFactory.getLogger(NettyClient.class);

    private static final Bootstrap bootstrap;
    private CommonSerializer serializer;
    private ServiceDiscovery serviceDiscovery;
    private static final EventLoopGroup group;

    /**
     * 未指定则使用默认的序列化器
     */
    public NettyClient() {
        this(DEFAULT_SERIALIZER, new RandomLoadBalancer());
    }

    public NettyClient(LoadBalancer loadBalancer) {
        this(DEFAULT_SERIALIZER, loadBalancer);
    }

    public NettyClient(Integer serializer, LoadBalancer loadBalancer) {
        this.serviceDiscovery = new NacosServiceDiscovery(loadBalancer);
        this.serializer = CommonSerializer.getByCode(serializer);
    }

    public NettyClient(Integer serializer) {
        this(serializer, new RandomLoadBalancer());
    }

    /**
     * 静态代码块，首先设置好启动器的基本配置，并且只调用一次。
     */
    static {
        group = new NioEventLoopGroup();
        bootstrap = new Bootstrap();
        bootstrap.group(group)
                .channel(NioSocketChannel.class)
                .option(ChannelOption.SO_KEEPALIVE, true);
    }

    /**
     * 将rpcRequest发送给服务端并接收到返回的相应结果
     *
     * @param rpcRequest
     * @return
     */
    @Override
    public Object sendRequest(RpcRequest rpcRequest) {
        if (serializer == null) {
            logger.error("未指定序列化器");
            throw new RpcException(RpcError.SERIALIZER_NOT_FOUND);
        }
        AtomicReference<Object> result = new AtomicReference<>(null);
        try {
            InetSocketAddress inetSocketAddress = serviceDiscovery.discoveryService(rpcRequest.getInterfaceName());
            Channel channel = ChannelProvider.get(inetSocketAddress, serializer);
            if (!channel.isActive()) {
                group.shutdownGracefully();
                return null;
            }
            // 异步方法进行监听，当发送数据后判断是否发送成功，如果成功的话打印成功的日志，否则打印失败的日志
            // 这里的发送时非阻塞的，判断是否发送成功但不能得到数据，
            channel.writeAndFlush(rpcRequest).addListener(future1 -> {
                if (future1.isSuccess()) {
                    logger.info(String.format("客户端发送消息：%s", rpcRequest.toString()));
                } else {
                    logger.error("发送消息时有错误发生：", future1.cause());
                }
            });
            channel.closeFuture().sync();
            // 这里通过获取到key 的方式获取到返回结果。
            AttributeKey<RpcResponse> key = AttributeKey.valueOf("rpcResponse" + rpcRequest.getRequestId());
            RpcResponse rpcResponse = channel.attr(key).get();
            RpcMessageChecker.check(rpcRequest, rpcResponse);
            result.set(rpcResponse.getData());

        } catch (InterruptedException e) {
            logger.error("发送消息时有错误发生：", e);
            Thread.currentThread().interrupt();
        }
        return result.get();
    }
}
