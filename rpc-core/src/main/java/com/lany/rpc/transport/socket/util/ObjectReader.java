package com.lany.rpc.transport.socket.util;

import com.lany.rpc.entity.RpcRequest;
import com.lany.rpc.entity.RpcResponse;
import com.lany.rpc.enumeration.PackageType;
import com.lany.rpc.enumeration.RpcError;
import com.lany.rpc.exception.RpcException;
import com.lany.rpc.serializer.CommonSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

/**
 * @author liuyanyan
 * @date 2021/12/28 11:25
 */
public class ObjectReader {
    private static final Logger logger = LoggerFactory.getLogger(ObjectReader.class);
    private static final int MAGIC_NUMBER = 0xCAFEBABE;

    protected Object readObject(InputStream in) throws Exception {
        // 首先读取第一部分判断是否是协议包
        // 读四个字节就是一个int的长度
        byte[] bytes = new byte[4];
        in.read(bytes);
        int magic = bytesToInt(bytes);
        if (magic != MAGIC_NUMBER) {
            logger.error("不识别的协议包：{}", magic);
            throw new RpcException(RpcError.UNKNOW_PROROCOL);
        }
        // 接着读取数据类型部分
        in.read(bytes);
        int packageCode = bytesToInt(bytes);
        Class<?> packageClass;
        if (packageCode == PackageType.REQUEST_PACK.getCode()) {
            packageClass = RpcRequest.class;
        } else if (packageCode == PackageType.RESPONSE_PACK.getCode()) {
            packageClass = RpcResponse.class;
        } else {
            logger.error("不识别的数据包：{}", packageCode);
            throw new RpcException(RpcError.UNKNOWN_PACKAGE_TYPE);
        }
        // 读取序列化协议部分
        in.read(bytes);
        int serializerCode = bytesToInt(bytes);
        CommonSerializer serializer = CommonSerializer.getByCode(serializerCode);
        if (serializer == null) {
            logger.error("不识别的反序列化器：{}", serializerCode);
            throw new RpcException(RpcError.UNKNOWN_SERIALIZER);
        }
        // 读取数据长度
        in.read(bytes);
        int length = bytesToInt(bytes);
        byte[] lengthBytes = new byte[length];
        // 读取数据
        in.read(lengthBytes);
        // 反序列化数据
        return serializer.deserialize(bytes, packageClass);
    }

    public static int bytesToInt(byte[] src) {
        int value;
        value = ((src[0] & 0xFF) << 24)
                | ((src[1] & 0xFF) << 16)
                | ((src[2] & 0xFF) << 8)
                | (src[3] & 0xFF);
        return value;
    }
}
