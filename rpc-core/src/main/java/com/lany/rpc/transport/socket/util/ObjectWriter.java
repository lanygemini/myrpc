package com.lany.rpc.transport.socket.util;

import com.lany.rpc.entity.RpcRequest;
import com.lany.rpc.enumeration.PackageType;
import com.lany.rpc.serializer.CommonSerializer;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author liuyanyan
 * @date 2021/12/28 11:31
 */
public class ObjectWriter {
    private static final int MAGIC_NUMBER = 0xCAFEBABE;

    private final CommonSerializer serializer;

    public ObjectWriter(CommonSerializer serializer) {
        this.serializer = serializer;
    }

    public static void writeObject(OutputStream out, Object o, CommonSerializer serializer) throws IOException {
        // 第一部分
        out.write(intToBytes(MAGIC_NUMBER));
        // 写入第二部分
        if (o instanceof RpcRequest) {
            out.write(intToBytes(PackageType.REQUEST_PACK.getCode()));
        } else {
            out.write(intToBytes(PackageType.RESPONSE_PACK.getCode()));
        }
        // 写入第三部分
        out.write(intToBytes(serializer.getCode()));
        byte[] bytes = serializer.serializer(o);
        // 写入数据的长度
        out.write(intToBytes(bytes.length));
        // 写入数据
        out.write(bytes);
        out.flush();
    }

    private static byte[] intToBytes(int value) {
        byte[] src = new byte[4];
        src[0] = (byte) ((value >> 24) & 0xFF);
        src[1] = (byte) ((value >> 16) & 0xFF);
        src[2] = (byte) ((value >> 8) & 0xFF);
        src[3] = (byte) (value & 0xFF);
        return src;
    }
}
