package com.lany.rpc.transport.socket.server;

import com.lany.rpc.handler.RequestHandler;
import com.lany.rpc.RpcServer;
import com.lany.rpc.enumeration.RpcError;
import com.lany.rpc.exception.RpcException;
import com.lany.rpc.hook.ShutdownHook;
import com.lany.rpc.provider.DefaultLocalServiceProvider;
import com.lany.rpc.provider.LocalServiceProvider;
import com.lany.rpc.registry.NacosServiceRegistry;
import com.lany.rpc.registry.ServiceRegistry;
import com.lany.rpc.serializer.CommonSerializer;
import com.lany.rpc.factory.ThreadPoolFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.*;

/**
 * @author liuyanyan
 * @date 2021/12/21 15:37
 */
public class SocketServer implements RpcServer {

    private static final Logger logger = LoggerFactory.getLogger(SocketServer.class);

    private final ExecutorService threadPool;
    private final RequestHandler requestHandler = new RequestHandler();
    private final String host;
    private final int port;

    private ServiceRegistry serviceRegistry;
    private LocalServiceProvider serviceProvider;
    private CommonSerializer serializer;

    /**
     * 初始化线程池，当有连接时就新建一个线程去执行
     */
    public SocketServer(String host, int port) {
        this(host, port, DEFAULT_SERIALIZER);
    }

    public SocketServer(String host, int port, int serializer) {
        this.host = host;
        this.port = port;
        this.serviceRegistry = new NacosServiceRegistry();
        this.serviceProvider = new DefaultLocalServiceProvider();
        this.threadPool = ThreadPoolFactory.createDefaultThreadPool("socket-rpc-server");
        this.serializer = CommonSerializer.getByCode(serializer);
    }

    @Override
    public <T> void publishService(T service, Class<T> serviceClass) {
        if (serializer == null) {
            logger.error("未设置序列化器");
            throw new RpcException(RpcError.SERIALIZER_NOT_FOUND);
        }
        serviceProvider.addServiceProvider(service, serviceClass);
        serviceRegistry.register(serviceClass.getCanonicalName(), new InetSocketAddress(host, port));
    }

    /**
     * 有连接建立时创建一个线程去执行 进行数据的传输
     */
    @Override
    public void start() {
        if (serializer == null) {
            logger.error("未指定序列化器");
            throw new RpcException(RpcError.SERIALIZER_NOT_FOUND);
        }
        try (ServerSocket serverSocket = new ServerSocket()) {
            serverSocket.bind(new InetSocketAddress(host, port));
            logger.info("服务器启动...");
            ShutdownHook.getShutdownHook().addClearAllHook();
            Socket socket;
            while ((socket = serverSocket.accept()) != null) {
                logger.info("客户端连接！Ip为：{} port为：{}", socket.getInetAddress(), socket.getPort());
                threadPool.execute(new SocketRequestHandlerThread(socket, requestHandler, serializer));
            }
            threadPool.shutdown();
        } catch (IOException e) {
            logger.error("连接时有错误发生：", e);
        }
    }
}
