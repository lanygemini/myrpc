package com.lany.rpc.transport.socket.server;

import com.lany.rpc.handler.RequestHandler;
import com.lany.rpc.entity.RpcRequest;
import com.lany.rpc.serializer.CommonSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * @author liuyanyan
 * @date 2021/12/21 15:37
 */
public class SocketRequestHandlerThread implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(SocketRequestHandlerThread.class);

    private Socket socket;
    private RequestHandler requestHandler;
    private CommonSerializer serializer;

    public SocketRequestHandlerThread(Socket socket, RequestHandler requestHandler, CommonSerializer serializer) {
        this.socket = socket;
        this.requestHandler = requestHandler;
        this.serializer = serializer;
    }

    @Override
    public void run() {
        try (ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream())) {
            RpcRequest rpcRequest = (RpcRequest) objectInputStream.readObject();
            // Object service = localServiceProvider.getServiceProvider(rpcRequest.getInterfaceName());
            // 这里处理数据交给了处理器去执行
            Object result = requestHandler.handle(rpcRequest);
            // 将返回的数据封装进RpcResponse中写入到缓冲区中
            objectOutputStream.writeObject(result);
            // 将缓冲区数据输入都流中
            objectOutputStream.flush();
        } catch (IOException | ClassNotFoundException e) {
            logger.error("调用或发送时有错误发生：", e);
        }


    }
}
