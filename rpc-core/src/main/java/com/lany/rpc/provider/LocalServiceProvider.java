package com.lany.rpc.provider;

/**
 * @author liuyanyan
 * @date 2021/12/22 14:10
 */
public interface LocalServiceProvider {

    <T> void addServiceProvider(T service,Class<T> serviceClass);

    Object getServiceProvider(String serviceName);

}
