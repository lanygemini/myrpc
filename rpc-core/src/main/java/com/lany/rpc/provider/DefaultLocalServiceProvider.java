package com.lany.rpc.provider;

import com.lany.rpc.enumeration.RpcError;
import com.lany.rpc.exception.RpcException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author liuyanyan
 * @date 2021/12/22 14:31
 */
public class DefaultLocalServiceProvider implements LocalServiceProvider {
    private static final Logger logger = LoggerFactory.getLogger(DefaultLocalServiceProvider.class);

    /**
     * key：接口名
     * value：Object为service实例
     */
    private static Map<String, Object> serviceMap = new ConcurrentHashMap<>();
    /**
     * set中存储的是已经注册过的服务
     */
    private static Set<String> registeredService = ConcurrentHashMap.newKeySet();

    /**
     * 因为客户端是通过接口进行调用的，因此这里将一个接口只对应一个实现类，
     * 向本地注册表中注册服务
     *
     * @param service
     * @param <T>
     */
    @Override
    public <T> void addServiceProvider(T service, Class<T> serviceClass) {
        // 获取到实例的名字
        String serviceName = serviceClass.getCanonicalName();
        // 如果该服务已经注册过了，直接返回，否则继续往下执行
        if (registeredService.contains(serviceName)) {
            return;
        }
        registeredService.add(serviceName);
        serviceMap.put(serviceName, service);
        logger.info("向接口：{}注册服务：{}", service.getClass().getInterfaces(), serviceName);
    }

    /**
     * 从本地注册表中饭后服务实例
     *
     * @param serviceName
     * @return
     */
    @Override
    public Object getServiceProvider(String serviceName) {
        Object service = serviceMap.get(serviceName);
        if (service == null) {
            throw new RpcException(RpcError.SERVICE_NOT_FOUND);
        }
        return service;
    }
}
