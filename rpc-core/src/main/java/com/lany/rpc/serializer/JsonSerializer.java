package com.lany.rpc.serializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lany.rpc.entity.RpcRequest;
import com.lany.rpc.enumeration.SerializerCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * @author jinchen
 * @version 1.0
 * @date 2021/12/25 15:50
 * <p>
 * Json格式的序列化器
 * JSON序列化工具使用的jackson，因此需要引入相应的依赖
 */
public class JsonSerializer implements CommonSerializer {

    private static final Logger logger = LoggerFactory.getLogger(JsonSerializer.class);

    private ObjectMapper objectMapper = new ObjectMapper();

    /**
     * 序列化
     *
     * @param object
     * @return
     */
    @Override
    public byte[] serializer(Object object) {
        try {
            // 将Object值序列化为字节数组
            return objectMapper.writeValueAsBytes(object);
        } catch (JsonProcessingException e) {
            logger.error("序列化时有错误发生：{}", e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 反序列化
     *
     * @param bytes
     * @param clazz
     * @return
     */
    @Override
    public Object deserialize(byte[] bytes, Class<?> clazz) {
        try {
            // 根据clazz将bytes反序列换为对应的实例，还需要判断反序列化后实例类型是否正确
            // 因为RepRequest中有一个Object的数据，进行反序列时可能会失败，
            Object obj = objectMapper.readValue(bytes, clazz);
            if (obj instanceof RpcRequest) {
                obj = handleRequest(obj);
            }
            return obj;
        } catch (IOException e) {
            logger.error("反序列化时有错误发生：{}", e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 由于使用JSON序列化和反序列化Object数组，无法保证反序列化后仍然为原实例类型
     * 需要重新判断
     *
     * @param obj
     * @return
     * @throws Exception
     */
    private Object handleRequest(Object obj) throws IOException {
        RpcRequest rpcRequest = (RpcRequest) obj;
        for (int i = 0; i < rpcRequest.getParamTypes().length; i++) {
            Class<?> clazz = rpcRequest.getParamTypes()[i];
            // 确定此Class对象表示的类或接口是否与指定的Class参数表示的类或接口相同，
            // 或者是其超类或超接口。 如果是，则返回true ； 否则返回false 。
            if (!clazz.isAssignableFrom(rpcRequest.getParameters()[i].getClass())) {
                // 如果不对应，则将这个参数重新反序列化为对应的参数类型。
                byte[] bytes = objectMapper.writeValueAsBytes(rpcRequest.getParameters()[i]);
                rpcRequest.getParameters()[i] = objectMapper.readValue(bytes, clazz);
            }
        }
        return rpcRequest;
    }

    @Override
    public int getCode() {
        return SerializerCode.valueOf("JSON").getCode();
    }
}
