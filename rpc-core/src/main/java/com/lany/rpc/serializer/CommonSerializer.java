package com.lany.rpc.serializer;

/**
 * @author jinchen
 * @version 1.0
 * @date 2021/12/25 15:48
 * <p>
 * 通用的反序列化接口
 */
public interface CommonSerializer {

    Integer KRYO_SERIALIZER = 0;
    Integer JSON_SERIALIZER = 1;
    Integer HESSIAN_SERIALIZER = 2;
    Integer PROTOBUF_SERIALIZER = 3;

    byte[] serializer(Object object);

    Object deserialize(byte[] bytes, Class<?> clazz) throws Exception;

    int getCode();

    static CommonSerializer getByCode(int code) {
        switch (code) {
            case 0:
                return new KryoSerializer();
            case 1:
                return new JsonSerializer();
            default:
                return null;
        }
    }

}
