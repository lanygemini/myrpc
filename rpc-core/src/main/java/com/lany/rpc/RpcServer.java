package com.lany.rpc;

import com.lany.rpc.serializer.CommonSerializer;

/**
 * @author jinchen
 * @version 1.0
 * @date 2021/12/25 12:43
 */
public interface RpcServer {

    int DEFAULT_SERIALIZER = CommonSerializer.KRYO_SERIALIZER;

    void start();

    <T> void publishService(T service, Class<T> serviceClass);
}
