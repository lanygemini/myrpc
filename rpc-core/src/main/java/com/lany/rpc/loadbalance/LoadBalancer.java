package com.lany.rpc.loadbalance;

import com.alibaba.nacos.api.naming.pojo.Instance;

import java.util.List;

/**
 * @author jinchen
 * @version 1.0
 * @date 2022/1/3 11:03
 */
public interface LoadBalancer {

    Instance select(List<Instance> instances);

}
