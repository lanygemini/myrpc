package com.lany.rpc.codec;

import com.lany.rpc.entity.RpcRequest;
import com.lany.rpc.enumeration.PackageType;
import com.lany.rpc.serializer.CommonSerializer;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * @author jinchen
 * @version 1.0
 * @date 2021/12/25 15:45
 * <p>
 * CommonEncoder就是将request或者response包装成协议包
 * 协议分为五个部分：
 * magicNumber表示这是一个协议包
 * packageType表示这是一个请求还是响应
 * SerializerType表示序列化的协议类型
 * DataLength表示实际数据的长度，防止粘包
 * 最后为数据部分
 * +---------------+---------------+-----------------+-------------+
 * |  Magic Number |  Package Type | Serializer Type | Data Length |
 * |    4 bytes    |    4 bytes    |     4 bytes     |   4 bytes   |
 * +---------------+---------------+-----------------+-------------+
 * |                          Data Bytes                           |
 * |                   Length: ${Data Length}                      |
 * +---------------------------------------------------------------+
 */
public class CommonEncoder extends MessageToByteEncoder {

    private static final int MAGIC_NUMBER = 0xCAFEBABE;

    private final CommonSerializer serializer;

    public CommonEncoder(CommonSerializer serializer) {
        this.serializer = serializer;
    }

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, Object o, ByteBuf byteBuf) throws Exception {
        // 第一部分
        byteBuf.writeInt(MAGIC_NUMBER);
        // 写入第二部分
        if (o instanceof RpcRequest) {
            byteBuf.writeInt(PackageType.REQUEST_PACK.getCode());
        } else {
            byteBuf.writeInt(PackageType.RESPONSE_PACK.getCode());
        }
        // 写入第三部分
        byteBuf.writeInt(serializer.getCode());
        byte[] bytes = this.serializer.serializer(o);
        // 写入数据的长度
        byteBuf.writeInt(bytes.length);
        // 写入数据
        byteBuf.writeBytes(bytes);
    }
}
