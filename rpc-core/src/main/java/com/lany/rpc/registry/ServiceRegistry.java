package com.lany.rpc.registry;

import java.net.InetSocketAddress;

/**
 * @author liuyanyan
 * @date 2021/12/30 14:18
 * 服务注册接口
 */
public interface ServiceRegistry {

    /**
     * 将提供服务的地址注册进来
     *
     * @param serviceName
     * @param inetSocketAddress
     */
    void register(String serviceName, InetSocketAddress inetSocketAddress);

}
