package com.lany.rpc.registry;

import java.net.InetSocketAddress;

/**
 * @author liuyanyan
 * @date 2021/12/30 17:12
 * 服务发现的接口
 */
public interface ServiceDiscovery {
    /**
     * 返回提供服务的服务器地址
     *
     * @param serviceName
     * @return
     */
    InetSocketAddress discoveryService(String serviceName);
}
