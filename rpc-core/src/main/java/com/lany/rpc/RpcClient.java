package com.lany.rpc;

import com.lany.rpc.entity.RpcRequest;
import com.lany.rpc.serializer.CommonSerializer;

/**
 * @author jinchen
 * @version 1.0
 * @date 2021/12/25 12:43
 */
public interface RpcClient {
    // 默认的序列化器为kryo
    int DEFAULT_SERIALIZER = CommonSerializer.KRYO_SERIALIZER;

    Object sendRequest(RpcRequest rpcRequest);
}
