# RPC框架实战之手写RPC框架



项目地址：[myrpc: 一个手写rpc框架项目 (gitee.com)](https://gitee.com/lanygemini/myrpc)

通过自己动手实践来巩固对RPC的学习，通过也可以更加深入的理解RPC原理。

首先第一章简单实现一个RPC，并后面逐步对该RPC进行完善，增加更多的功能。

## 第一章

第一部分首先实现简单的RPC远程通信，流程如下：

- 客户端调用接口的方法，通过代理将要调用的方法信息传输给服务端
- 服务端通过socket监听，当接收到数据后，就创建一个线程去执行
- 通过客户端传输过来的数据反射找到对应的方法，并执行获取到对应的数据
- 将数据封装进response中返回给客户端
- 客户端收到数据后打印。

因为是简单的实现，因此直接指定了服务端的地址，后续会进行优化完善。

让我们开始吧！

项目的整体模块如下：

- myrpc
  - rpc-api：接口相关的类
  - rpc-common：通用模块，例如服务端和消费端传输的RpcRequest
  - rpc-core：项目的核心模块
  - test-client：客户端相关的类
  - test-server：服务端相关的类

### 定义接口

首先定义接口，也就客户端调用的接口

```java
package com.lany.api;

/**
 * @author liuyanyan
 * @date 2021/12/21 14:19
 * 声明一个可以被外界调用的接口
 */
public interface HelloService {
    String hello(HelloObject helloObject);
}
```

参数我们用的是对象，因此还需要创建HelloObject,同时需要实现Serializable接口，因为后面传输时需要接口类型，因此需要序列化,没有序列化会报错。

```java
package com.lany.api;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * @author liuyanyan
 * @date 2021/12/21 14:20
 */
@Data
@AllArgsConstructor
public class HelloObject implements Serializable {
    private Integer id;
    private String message;
}
```

### 传输对象

接着需要定义传输对象

定义该对象来让服务端唯一确定一个方法，因此需要的参数有接口的名字、方法的名字、参数列表以及参数的类型。

```java
package com.lany.rpc.entity;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author liuyanyan
 * @date 2021/12/21 14:49
 * 数据传输时服务端和消费端通过该对象来确定消费端调用的是哪个方法
 */
@Data
@Builder
public class RpcRequest implements Serializable {
    /**
     * 待调用的接口名称
     */
    private String interfaceName;
    /**
     * 待调用方法名称
     */
    private String methodName;
    /**
     * 调用方法的参数
     */
    private Object[] parameters;
    /**
     * 调用方法的参数类型
     */
    private Class<?>[] paramTypes;
}
```

服务端通过这个对象找到对应的方法后执行将结果封装进RpcResponse中返回给消费因此还需要一个RpcResponse来接收方法执行的结果，返回时成功还是失败。

```java
package com.lany.rpc.entity;

import com.lany.rpc.enumeration.ResponseCode;
import lombok.Data;

import java.io.Serializable;

/**
 * @author liuyanyan
 * @date 2021/12/21 14:57
 * 服务调用成功或者失败返回的响应信息
 */
@Data
public class RpcResponse<T> implements Serializable {

    /**
     * 响应状态码
     */
    private Integer statusCode;

    /**
     * 响应状态补充信息
     */
    private String message;

    /**
     * 响应数据
     */
    private T data;

    /**
     * 返回成功的相应数据
     *
     * @param data
     * @param <T>
     * @return
     */
    public static <T> RpcResponse<T> success(T data) {
        RpcResponse<T> response = new RpcResponse<>();
        response.setStatusCode(ResponseCode.SUCCESS.getCode());
        response.setData(data);
        return response;
    }

    /**
     * 返回失败的响应信息
     *
     * @param code
     * @param <T>
     * @return
     */
    public static <T> RpcResponse<T> fail(ResponseCode code) {
        RpcResponse<T> response = new RpcResponse<>();
        response.setStatusCode(code.getCode());
        response.setMessage(code.getMessage());
        return response;
    }
}
```

### 动态代理

消费端通过动态代理来将想要调用的数据传输给服务端，让消费端只管调用，而不用管具体的怎么实现，就像调用本地方法一样。这里使用jdk动态代理来实现。

```java
package com.lany.rpc.client;

import com.lany.rpc.entity.RpcRequest;
import com.lany.rpc.entity.RpcResponse;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author liuyanyan
 * @date 2021/12/21 15:13
 */
public class RpcClientProxy implements InvocationHandler {

    private String host;
    private int port;

    public RpcClientProxy(String host, int port) {
        this.host = host;
        this.port = port;
    }

    @SuppressWarnings("unchecked")
    public <T> T getProxy(Class<T> clazz) {
        return (T) Proxy.newProxyInstance(clazz.getClassLoader(), new Class[]{clazz}, this);

    }

    /**
     * 代理类在被调用时的动作，通过代理将RPCRequest对象发送出去
     * 然后获取到返回的数据
     * 用户只需要调用就行了，代理来帮忙发送数据给服务端。
     *
     * @param proxy
     * @param method
     * @param args
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        RpcRequest rpcRequest = RpcRequest.builder()
                .interfaceName(method.getDeclaringClass().getName())
                .methodName(method.getName())
                .parameters(args)
                .paramTypes(method.getParameterTypes())
                .build();
        RpcClient rpcClient = new RpcClient();
        return ((RpcResponse) rpcClient.sendRequest(rpcRequest, host, port)).getData();
    }
}
```

封装了一个getProxy()方法，来返回代理对象。在代理对象调用具体的方法的时候调用了invoke()方法来把消费端想要调用的方法数据传输给服务端并接受到服务端传回来的数据。

具体与服务端通信的逻辑在RpcClient类中的sendRequest方法中实现。

```java
package com.lany.rpc.client;

import com.lany.rpc.entity.RpcRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;


/**
 * @author liuyanyan
 * @date 2021/12/21 15:12
 */
public class RpcClient {
    private static final Logger logger = LoggerFactory.getLogger(RpcClient.class);

    /**
     * 通过socket发送给服务端，并接受到返回的数据
     * 通过Java的序列化方式在socket中传输
     *
     * @param rpcRequest
     * @param host
     * @param port
     * @return
     */
    public Object sendRequest(RpcRequest rpcRequest, String host, int port) {
        try (Socket socket = new Socket(host, port)) {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
            objectOutputStream.writeObject(rpcRequest);
            objectOutputStream.flush();
            return objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            logger.error("调用时有错误发生：" + e);
            return null;
        }
    }
}
```

### 反射调用

服务端通过反射进行调用对应的方法

服务端一直监听9000端口，当有请求连接时通过线程池创建线程让其执行通信的逻辑

```java
package com.lany.rpc.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.*;

/**
 * @author liuyanyan
 * @date 2021/12/21 15:37
 */
public class RpcServer {

    private final ExecutorService threadPool;
    private static final Logger logger = LoggerFactory.getLogger(RpcServer.class);

    /**
     * 初始化线程池，当有连接时就新建一个线程去执行
     */
    public RpcServer() {
        // 核心线程数
        int corePoolSize = 5;
        // 最大线程数
        int maximumPoolSize = 50;
        // 空闲线程的等待时间
        long keepAliveTime = 60;
        // 阻塞队列
        BlockingQueue<Runnable> workingQueue = new ArrayBlockingQueue<>(100);
        // 线程工厂
        ThreadFactory threadFactory = Executors.defaultThreadFactory();

        threadPool = new ThreadPoolExecutor(
                corePoolSize,
                maximumPoolSize,
                keepAliveTime,
                TimeUnit.SECONDS,
                workingQueue,
                threadFactory);
    }

    /**
     * 有连接建立时创建一个线程去执行 进行数据的传输
     *
     * @param service
     * @param port
     */
    public void register(Object service, int port) {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            logger.info("服务器正在启动...");
            Socket socket;
            while ((socket = serverSocket.accept()) != null) {
                logger.info("客户端连接！Ip为：" + socket.getInetAddress());
                threadPool.execute(new WorkerThread(socket, service));
            }
        } catch (IOException e) {
            logger.error("连接时有错误发生：", e);
        }
    }
}
```

这里RpcServer咱叔只能注册一个接口，只能对外提供一个接口的调用方法，下一章会进行优化。

### 测试运行



消费端测试代码：

```java
package com.lany.test;

import com.lany.api.HelloObject;
import com.lany.api.HelloService;
import com.lany.rpc.client.RpcClientProxy;

/**
 * @author liuyanyan
 * @date 2021/12/21 16:04
 */
public class TestClient {
    public static void main(String[] args) {
        RpcClientProxy rpcClientProxy = new RpcClientProxy("127.0.0.1", 9000);
        HelloService helloService = rpcClientProxy.getProxy(HelloService.class);
        HelloObject helloObject = new HelloObject(12, "this is a message");
        String hello = helloService.hello(helloObject);
        System.out.println(hello);
    }
}
```

服务端测试代码：

```java
package com.lany.test;

import com.lany.rpc.server.RpcServer;

/**
 * @author liuyanyan
 * @date 2021/12/21 16:04
 */
public class TestServer {

    public static void main(String[] args) {
        HelloServiceImpl helloService = new HelloServiceImpl();
        RpcServer rpcServer = new RpcServer();
        rpcServer.register(helloService, 9000);

    }
}
```

## 第二章

第一章中我们的服务端测试代码中只能注册一个服务，这一章对其进行优化，**可以注册多个服务**。

### 服务注册表

首先需要一个注册表来存放注册的服务，并且可以返回需要的服务实例。

```java
package com.lany.rpc.registry;
/**
 * @author liuyanyan
 * @date 2021/12/22 14:10
 */
public interface RpcRegistry {

    <T> void register(T service);

    Object getService(String serviceName);
}
```

对该接口进行默认的实现。

```java
package com.lany.rpc.registry;

import com.lany.rpc.enumeration.RpcError;
import com.lany.rpc.exception.RpcException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author liuyanyan
 * @date 2021/12/22 14:31
 */
public class DefaultRpcRegistry implements RpcRegistry {
    private static final Logger logger = LoggerFactory.getLogger(DefaultRpcRegistry.class);

    /**
     * key：接口名
     * value：Object为service实例
     */
    private static Map<String, Object> serviceMap = new ConcurrentHashMap<>();
    /**
     * set中存储的是已经注册过的服务
     */
    private static Set<String> registeredService = ConcurrentHashMap.newKeySet();

    /**
     * 因为客户端是通过接口进行调用的，因此这里将一个接口只对应一个实现类，
     * @param service
     * @param <T>
     */
    @Override
    public <T> void register(T service) {
        // 获取到实例的名字
        String serviceName = service.getClass().getCanonicalName();
        // 如果该服务已经注册过了，直接返回，否则继续往下执行
        if (registeredService.contains(serviceName)) {
            return;
        }
        registeredService.add(serviceName);
        // 获取到该服务实现的所有接口
        Class<?>[] interfaces = service.getClass().getInterfaces();
        // 处理异常，如果长度为0说明该服务没有实现任何接口
        if (interfaces.length == 0) {
            throw new RpcException(RpcError.SERVICE_NOT_IMPLEMENT_ANY_INTERFACE);
        }
        // 将实现的接口名和该服务存入map中，如果一个服务实现了多个接口，
        // 则这几个接口对应的实现都为该服务。
        for (Class<?> i : interfaces) {
            serviceMap.put(i.getCanonicalName(), service);
        }

    }

    @Override
    public Object getService(String serviceName) {
        Object service = serviceMap.get(serviceName);
        if (service == null) {
            throw new RpcException(RpcError.SERVICE_NOT_FOUND);
        }
        return service;
    }
}
```

serviceMap用来存储注册进来的服务信息，registeredService用来存储已经注册过的服务，防止重复注册。因为serviceMap中存储的接口的名字，而不是实现类的名字，所以还需要一个set来记录存储过的实例信息。

同时因为服务端是通过接口调用的，因此这里将一个接口只对应一个实现类。

当通过名字获取实例时，直接将map中的value返回即可。

### 数据处理

上面这些完成之后就需要对RpcServer进行修改，将注册中心与RpcServer进行关联，而不是第一章中的实例与RpcServer关联。

```java
package com.lany.rpc.server;

import com.lany.rpc.registry.RpcRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.spi.ServiceRegistry;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.*;

/**
 * @author liuyanyan
 * @date 2021/12/21 15:37
 */
public class RpcServer {

    private static final Logger logger = LoggerFactory.getLogger(RpcServer.class);

    private final ExecutorService threadPool;
    // 核心线程数
    private static final int corePoolSize = 5;
    // 最大线程数
    private static final int maximumPoolSize = 50;
    // 空闲线程的等待时间
    private static final long keepAliveTime = 60;
    /**
     * 阻塞队列
     */
    BlockingQueue<Runnable> workingQueue = new ArrayBlockingQueue<>(100);
    /**
     * 线程工厂
     */
    ThreadFactory threadFactory = Executors.defaultThreadFactory();

    private final RequestHandler requestHandler = new RequestHandler();
    private final RpcRegistry rpcRegistry;

    /**
     * 初始化线程池，当有连接时就新建一个线程去执行
     */
    public RpcServer(RpcRegistry rpcRegistry) {
        this.rpcRegistry = rpcRegistry;
        this.threadPool = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime,
                TimeUnit.SECONDS, workingQueue, threadFactory);
    }

    /**
     * 有连接建立时创建一个线程去执行 进行数据的传输
     *
     * @param port
     */
    public void start(int port) {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            logger.info("服务器启动...");
            Socket socket;
            while ((socket = serverSocket.accept()) != null) {
                logger.info("客户端连接！Ip为：{} port为：{}", socket.getInetAddress(), socket.getPort());
                threadPool.execute(new RequestHandlerThread(socket, requestHandler, rpcRegistry));
            }
            threadPool.shutdown();
        } catch (IOException e) {
            logger.error("连接时有错误发生：", e);
        }
    }
}
```

创建RequestHandler和RequestHandleThread分别用来执行对应的方法并返回结果和创建客户端的连接传输数据。

对应代码如下：

```java
package com.lany.rpc.server;

import com.lany.rpc.entity.RpcRequest;
import com.lany.rpc.entity.RpcResponse;
import com.lany.rpc.registry.RpcRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.spi.ServiceRegistry;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Socket;

/**
 * @author liuyanyan
 * @date 2021/12/21 15:37
 */
public class RequestHandlerThread implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(RequestHandlerThread.class);

    private Socket socket;
    private RequestHandler requestHandler;
    private RpcRegistry rpcRegistry;

    public RequestHandlerThread(Socket socket, RequestHandler requestHandler, RpcRegistry rpcRegistry) {
        this.socket = socket;
        this.requestHandler = requestHandler;
        this.rpcRegistry = rpcRegistry;
    }

    @Override
    public void run() {
        try (ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream())) {
            RpcRequest rpcRequest = (RpcRequest) objectInputStream.readObject();
            Object service = rpcRegistry.getService(rpcRequest.getInterfaceName());
            // 这里处理数据交给了处理器去执行
            Object result = requestHandler.handle(rpcRequest, service);
            // 将返回的数据封装进RpcResponse中写入到缓冲区中
            objectOutputStream.writeObject(result);
            // 将缓冲区数据输入都流中
            objectOutputStream.flush();
        } catch (IOException | ClassNotFoundException e) {
            logger.error("调用或发送时有错误发生：", e);
        }


    }
}
```



```java
package com.lany.rpc.server;

import com.lany.rpc.entity.RpcRequest;
import com.lany.rpc.entity.RpcResponse;
import com.lany.rpc.enumeration.ResponseCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author liuyanyan
 * @date 2021/12/22 15:13
 */
public class RequestHandler {
    private static final Logger logger = LoggerFactory.getLogger(RequestHandler.class);

    public Object handle(RpcRequest rpcRequest, Object service) {
        Object result = null;
        try {
            result = invokeTargetMethod(rpcRequest, service);
            logger.info("服务：{} 成功调用方法：{}", rpcRequest.getInterfaceName(), rpcRequest.getMethodName());
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            logger.error("调用或发送时有错误发生：", e);
        }
        return result;
    }

    /**
     * 通过反射执行目标方法
     *
     * @param rpcRequest
     * @param service
     * @return
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    private Object invokeTargetMethod(RpcRequest rpcRequest, Object service) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = null;
        try {
            method = service.getClass().getMethod(rpcRequest.getMethodName(), rpcRequest.getParamTypes());
        } catch (NoSuchMethodException | SecurityException e) {
            return RpcResponse.fail(ResponseCode.METHOD_NOT_FOUND);
        }
        return RpcResponse.success(method.invoke(service, rpcRequest.getParameters()));
    }
}
```

### 测试运行

客户端的测试代码没有变化，主要就是服务端的测试修改了。

```java
package com.lany.test;

import com.lany.rpc.registry.DefaultRpcRegistry;
import com.lany.rpc.registry.RpcRegistry;
import com.lany.rpc.server.RpcServer;

/**
 * @author liuyanyan
 * @date 2021/12/21 16:04
 */
public class TestServer {

    public static void main(String[] args) {
        HelloServiceImpl helloService = new HelloServiceImpl();
        HelloServiceImpl2 helloService2 = new HelloServiceImpl2();
        RpcRegistry rpcRegistry = new DefaultRpcRegistry();
        rpcRegistry.register(helloService);
        // 这里将service2注册后将helloservice的给覆盖了，导致每次都客户端都调用的是helloservice2
        rpcRegistry.register(helloService2);
        RpcServer rpcServer = new RpcServer(rpcRegistry);
        rpcServer.start(9000);

    }
}
```

testServer：

```
[main] INFO com.lany.rpc.server.RpcServer - 服务器启动...
[main] INFO com.lany.rpc.server.RpcServer - 客户端连接！Ip为：/127.0.0.1 port为：53812
[pool-1-thread-1] INFO com.lany.test.HelloServiceImpl2 - 接收到2：this is a message
[pool-1-thread-1] INFO com.lany.rpc.server.RequestHandler - 服务：com.lany.api.HelloService 成功调用方法：hello
```

testClient：

```
这时调用的返回值2，id2=12
```



## 第三章

**使用Netty传输数据**，使用效率更高的Nio方式传输数据，因为原先使用的是传统BIO方式，现在为了支持Netty方式，需要抽象出接口，增加扩展性，这样也满足了设计模式六大原则中的依赖倒置原则。

### 抽象为接口

RpcServer和RpcClient抽象为接口

NettyServer实现RpcServer，NettyClient实现RpcClient

```java
public interface RpcClient{
    Object sendRequest(RpcRequest rpcRequest);
}

public interface RpcServer{
    void start(int port);
}
```

### 导入Netty的依赖

```xml
<dependency>
    <groupId>io.netty</groupId>
    <artifactId>netty-all</artifactId>
    <version>4.1.70.Final</version>
</dependency>
```

### NettyServer类

```java
public class NettyServer implements RpcServer {

    private static final Logger logger = LoggerFactory.getLogger(NettyServer.class);

    /**
     * 启动并监听对应的端口，等待客户端的连接并处理数据
     *
     * @param port
     */
    @Override
    public void start(int port) {
        NioEventLoopGroup bossGroup = new NioEventLoopGroup();
        NioEventLoopGroup workGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap.group(bossGroup, workGroup)
                    .channel(NioServerSocketChannel.class)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .option(ChannelOption.SO_BACKLOG, 256)
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    .childOption(ChannelOption.TCP_NODELAY, true)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel socketChannel) throws Exception {
                            ChannelPipeline pipeline = socketChannel.pipeline();
                            pipeline.addLast(new CommonEncoder(new JsonSerializer()));
                            pipeline.addLast(new CommonDecoder());
                            pipeline.addLast(new NettyServerHander());
                        }
                    });
            ChannelFuture future = serverBootstrap.bind(port).sync();
            future.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            logger.error("启动服务器时有错误发生: ", e);
        } finally {
            bossGroup.shutdownGracefully();
            workGroup.shutdownGracefully();
        }

    }
}
```

#### ChannelOption.SO_BACKLOG

该设置为服务端接收连接的队列长度，如果队列已满，客户单连接将被拒绝。

服务端在处理客户端新连接请求时（三次握手）是顺序处理的，同一时间只能处理一个客户端连接，当有多个客户端连接到来的时候，服务端就会将不能处理的请求放在等待队列汇总等待，SO_BACKLOG设置的参数就是队列的大小。

服务端对完成第二次握手的连接放在一个队列（a队列）

如果完成了第三次握手，再把连接放在一个新队列中（b队列）

应用程序通过accept()方法取出握手成功的连接，然后系统会将该连接从队列中移除

a队列和b队列的长度总和为SO_BACKLOG设置的值，如果两个队列长度和大于SO_BACKLOG时，新连接将会被拒绝。

#### ChannelOption.SO_KEEPALIVE

该设置表示是否开启TCP心跳机制，true为连接保持心跳，默认为false，启用该功能后TCP会主动探测空闲连接的有效性，默认间隔为两小时。

#### ChannelOption.TCP_NODELAY

该设置如果为true表示立即发送数据，如果要求实时性，有数据发送时立马发送，就设置为true（关闭Nagle算法）；如果减少发送次数、减少网络交互，就设置为false（开启Nagle算法），当累积到一定大小的数据后在发送。

Nagle算法将小的碎片数据连接成更大的报文（或数据包）来最小化发送报文的数量，如果想要发送一些较小的报文，则需要禁用该算法。

#### ChildOption和ChildHandler

这两个是给workerGroup进行设置的，而Option和Handler是给bossGroup进行设置的。

当有一个新客户端连接到来的时候bossGroup就会为此连接初始化各种资源，然后从workerGroup中选出一个EventLoop绑定到此客户端连接中。服务端和客户端的交互过程就全在这个EventLoop中完成。

### NettyClient类

```java
public class NettyClient implements RpcClient {
    private static final Logger logger = LoggerFactory.getLogger(NettyClient.class);

    private String host;
    private int port;
    private static final Bootstrap bootstrap;

    public NettyClient(String host, int port) {
        this.host = host;
        this.port = port;
    }

    /**
     * 静态代码块，首先设置好启动器的基本配置，并且只调用一次。
     */
    static {
        NioEventLoopGroup group = new NioEventLoopGroup();
        bootstrap = new Bootstrap();
        bootstrap.group(group)
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        ChannelPipeline pipeline = socketChannel.pipeline();
                        pipeline.addLast(new CommonDecoder());
                        pipeline.addLast(new CommonEncoder(new JsonSerializer()));
                        pipeline.addLast(new NettyClientHandler());
                    }
                })
                .option(ChannelOption.SO_KEEPALIVE, true);
    }

    /**
     * 将rpcRequest发送给服务端并接收到返回的相应结果
     *
     * @param rpcRequest
     * @return
     */
    @Override
    public Object sendRequest(RpcRequest rpcRequest) {
        try {
            ChannelFuture future = bootstrap.connect(host, port).sync();
            logger.info("客户端连接到服务器：{}:{}", host, port);
            // 获取到连接的通道
            Channel channel = future.channel();
            if (channel != null) {
                // 异步方法进行监听，当发送数据后判断是否发送成功，如果成功的话打印成功的日志，否则打印失败的日志
                // 这里的发送时非阻塞的，判断是否发送成功但不能得到数据，
                channel.writeAndFlush(rpcRequest).addListener(future1 -> {
                    if (future1.isSuccess()) {
                        logger.info(String.format("客户端发送消息：%s", rpcRequest.toString()));
                    } else {
                        logger.error("发送消息时有错误发生：", future.cause());
                    }
                });
                channel.closeFuture().sync();
                // 这里通过获取到key 的方式获取到返回结果。
                AttributeKey<RpcResponse> key = AttributeKey.valueOf("rpcResponse");
                RpcResponse rpcResponse = channel.attr(key).get();
                return rpcResponse.getData();
            }
        } catch (InterruptedException e) {
            logger.error("发送消息时有错误发生：", e);
        }
        return null;
    }
}
```

### 自定义协议

在服务端和客户端的队列中都添加了一个自定义的编码器、解码器和处理器。

因为自定义了传输协议，因此需要自定义编码解码器

```java
/* CommonEncoder就是将request或者response包装成协议包
* 协议分为五个部分：
* magicNumber表示这是一个协议包
* packageType表示这是一个请求还是响应
* SerializerType表示序列化的协议类型
* DataLength表示实际数据的长度，防止粘包
* 最后为数据部分
* +---------------+---------------+-----------------+-------------+
* |  Magic Number |  Package Type | Serializer Type | Data Length |
* |    4 bytes    |    4 bytes    |     4 bytes     |   4 bytes   |
* +---------------+---------------+-----------------+-------------+
* |                          Data Bytes                           |
* |                   Length: ${Data Length}                      |
* +---------------------------------------------------------------+
 /
```

CommonDecoder需要实现ByteToMessageDecoder，同时实现decode方法

CommonIncoder需要实现MessageToByteEncoder，同时实现encode方法

#### CommonDecoder

```java
public class CommonDecoder extends ReplayingDecoder {

    private static final Logger logger = LoggerFactory.getLogger(CommonDecoder.class);

    private static final int MAGIC_NUMBER = 0xCAFEBABE;

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        // 首先读取第一部分判断是否是协议包
        int magic = in.readInt();
        if (magic != MAGIC_NUMBER) {
            logger.error("不识别的协议包：{}", magic);
            throw new RpcException(RpcError.UNKNOW_PROROCOL);
        }
        // 接着读取数据类型部分
        int packageCode = in.readInt();
        Class<?> packageClass;
        if (packageCode == PackageType.REQUEST_PACK.getCode()) {
            packageClass = RpcRequest.class;
        } else if (packageCode == PackageType.RESPONSE_PACK.getCode()) {
            packageClass = RpcResponse.class;
        } else {
            logger.error("不识别的数据包：{}", packageCode);
            throw new RpcException(RpcError.UNKNOWN_PACKAGE_TYPE);
        }
        // 读取序列化协议部分
        int serializerCode = in.readInt();
        CommonSerializer serializer = CommonSerializer.getByte(serializerCode);
        if (serializer == null) {
            logger.error("不识别的反序列化器：{}", serializerCode);
            throw new RpcException(RpcError.UNKNOWN_SERIALIZER);
        }
        // 读取数据长度
        int length = in.readInt();
        byte[] bytes = new byte[length];
        // 读取数据
        in.readBytes(bytes);
        // 反序列化数据
        Object obj = serializer.deserialize(bytes, packageClass);
        out.add(obj);

    }
}
```

#### CommonEncoder

```java
public class CommonEncoder extends MessageToByteEncoder {

    private static final int MAGIC_NUMBER = 0xCAFEBABE;

    private final CommonSerializer serializer;

    public CommonEncoder(CommonSerializer serializer) {
        this.serializer = serializer;
    }

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, Object o, ByteBuf byteBuf) throws Exception {
        // 第一部分
        byteBuf.writeInt(MAGIC_NUMBER);
        // 写入第二部分
        if (o instanceof RpcRequest) {
            byteBuf.writeInt(PackageType.REQUEST_PACK.getCode());
        } else {
            byteBuf.writeInt(PackageType.RESPONSE_PACK.getCode());
        }
        // 写入第三部分
        byteBuf.writeInt(serializer.getCode());
        byte[] bytes = this.serializer.serializer(o);
        // 写入数据的长度
        byteBuf.writeInt(bytes.length);
        // 写入数据
        byteBuf.writeBytes(bytes);
    }
}
```

#### 处理器

服务端处理器

```java
public class NettyServerHander extends SimpleChannelInboundHandler<RpcRequest> {

    private static final Logger logger = LoggerFactory.getLogger(NettyServerHander.class);

    private static RequestHandler requestHandler;

    private static RpcRegistry rpcRegistry;

    static {
        requestHandler = new RequestHandler();
        rpcRegistry = new DefaultRpcRegistry();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, RpcRequest msg) throws Exception {
        try {
            logger.info("服务器接收到请求：{}", msg);
            String interfaceName = msg.getInterfaceName();
            Object service = rpcRegistry.getService(interfaceName);
            Object result = requestHandler.handle(msg, service);
            ChannelFuture future = ctx.writeAndFlush(result);
            future.addListener(ChannelFutureListener.CLOSE);
        } finally {
            ReferenceCountUtil.release(msg);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error("处理过程调用时有错误发生：");
        cause.printStackTrace();
        ctx.close();
    }
}
```

客户端处理器

```java
public class NettyClientHandler extends SimpleChannelInboundHandler<RpcResponse> {
    private static final Logger logger = LoggerFactory.getLogger(NettyClientHandler.class);

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, RpcResponse msg) throws Exception {
        try {
            logger.info("客户端接收到消息：{}", msg);
            AttributeKey<RpcResponse> key = AttributeKey.valueOf("rpcResponse");
            ctx.channel().attr(key).set(msg);
            ctx.channel().close();
        } finally {
            ReferenceCountUtil.release(msg);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error("过程调用时有错误发生：");
        cause.printStackTrace();
        ctx.close();
    }
}
```

### 序列化接口

CommonSerializer

```java
public interface CommonSerializer {

    byte[] serializer(Object object);

    Object deserialize(byte[] bytes, Class<?> clazz) throws Exception;

    int getCode();

    static CommonSerializer getByte(int code) {
        switch (code) {
            case 1:
                return new JsonSerializer();
            default:
                return null;
        }
    }

}
```

实现了JSON的序列化器，序列化器使用的是jackson

```java
public class JsonSerializer implements CommonSerializer {

    private static final Logger logger = LoggerFactory.getLogger(JsonSerializer.class);

    private ObjectMapper objectMapper = new ObjectMapper();

    /**
     * 序列化
     *
     * @param object
     * @return
     */
    @Override
    public byte[] serializer(Object object) {
        try {
            // 将Object值序列化为字节数组
            return objectMapper.writeValueAsBytes(object);
        } catch (JsonProcessingException e) {
            logger.error("序列化时有错误发生：{}", e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 反序列化
     *
     * @param bytes
     * @param clazz
     * @return
     */
    @Override
    public Object deserialize(byte[] bytes, Class<?> clazz) {
        try {
            // 根据clazz将bytes反序列换为对应的实例，还需要判断反序列化后实例类型是否正确
            // 因为RepRequest中有一个Object的数据，进行反序列时可能会失败，
            Object obj = objectMapper.readValue(bytes, clazz);
            if (obj instanceof RpcRequest) {
                obj = handleRequest(obj);
            }
            return obj;
        } catch (IOException e) {
            logger.error("反序列化时有错误发生：{}", e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 由于使用JSON序列化和反序列化Object数组，无法保证反序列化后仍然为原实例类型
     * 需要重新判断
     *
     * @param obj
     * @return
     * @throws Exception
     */
    private Object handleRequest(Object obj) throws IOException {
        RpcRequest rpcRequest = (RpcRequest) obj;
        for (int i = 0; i < rpcRequest.getParamTypes().length; i++) {
            Class<?> clazz = rpcRequest.getParamTypes()[i];
            // 确定此Class对象表示的类或接口是否与指定的Class参数表示的类或接口相同，
            // 或者是其超类或超接口。 如果是，则返回true ； 否则返回false 。
            if (!clazz.isAssignableFrom(rpcRequest.getParameters()[i].getClass())) {
                // 如果不对应，则将这个参数重新反序列化为对应的参数类型。
                byte[] bytes = objectMapper.writeValueAsBytes(rpcRequest.getParameters()[i]);
                rpcRequest.getParameters()[i] = objectMapper.readValue(bytes, clazz);
            }
        }
        return rpcRequest;
    }

    @Override
    public int getCode() {
        return SerializerCode.valueOf("JSON").getCode();
    }
}
```

使用JSON序列化器无法保证反序列化后仍然为原实例类型，因此后面会增加性能更好的序列化器。

在RPCRequest中有一个字段是Object数组，是一个模糊的类型，在反序列化时会出现失败的现象，所以就需要另一个字段ParamTypes来获取Object数组中的每个实例的实际类，辅助反序列化，也就是handleRequet（）方法的作用。



## 第四章

增加Kryo序列化器，上一章中使用JSON序列化器有一个问题，就是如果反序列化的是对象中有Object类型的属性，反序列化会出错，通常会把Object属性直接反序列化为String类型，因此还需要其他参数辅助序列化，并且JSON序列化器是基于字符串（JSON串）的，占用空间大并且速度慢。

Kryo是一个快速高效的Java对象序列化器，特点就是高性能、高效、易用。它是基于字节的序列化，对空间利用率高，在网络传输时可以减小体积，并且在序列化时记录属性对象的类型信息，因此在反序列化时不会出现类型错误的情况。

### 引入依赖

需要引入Kryo依赖

```xml
<dependency>
    <groupId>com.esotericsoftware</groupId>
    <artifactId>kryo</artifactId>
    <version>4.0.2</version>
</dependency>
```

这里把Kryo作为默认的序列化器来使用，这里使用过switch的方式根据用户传入的编号返回对应的序列化器，这样其实不太友好，如果序列化器多的话，用户还要记忆每个序列化器对应的编号，后续可能会优化序列化器的使用方式。

```java
public interface CommonSerializer {

    byte[] serializer(Object object);

    Object deserialize(byte[] bytes, Class<?> clazz) throws Exception;

    int getCode();

    static CommonSerializer getByte(int code) {
        switch (code) {
            case 0:
                return new KryoSerializer();
            case 1:
                return new JsonSerializer();
            default:
                return null;
        }
    }

}
```

### KryoSerializer

接下来就是序列化器的实现了

```java
public class KryoSerializer implements CommonSerializer {

    private static final Logger logger = LoggerFactory.getLogger(KryoSerializer.class);

    /**
     * 因为kryo不是线程安全的，因此多线程的情况下可以考虑使用ThreadLocal或者池化
     */
    private static final ThreadLocal<Kryo> kryoThreadLocal = ThreadLocal.withInitial(() -> {
        Kryo kryo = new Kryo();
        // 注册序列化或反序列化类
        kryo.register(RpcResponse.class);
        kryo.register(RpcRequest.class);
        // 设置true如果出现多次则是序列化一次
        kryo.setReferences(true);
        // 未注册的类也可以进行反序列化
        kryo.setRegistrationRequired(false);
        return kryo;
    });

    @Override
    public byte[] serializer(Object object) {
        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
             // 序列化时先创建一个output对象，然后使用writeObject将object对象写入output中，在调用getBytes即可获得对象的字节数组。
             Output output = new Output(byteArrayOutputStream)) {
            Kryo kryo = kryoThreadLocal.get();
            kryo.writeObject(output, object);
            kryoThreadLocal.remove();
            return output.toBytes();
        } catch (IOException e) {
            logger.error("序列化时有错误发生：", e);
            throw new SerializeException("序列化时有错误发生");
        }
    }

    @Override
    public Object deserialize(byte[] bytes, Class<?> clazz) throws Exception {
        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
             Input input = new Input(byteArrayInputStream)) {
            // 反序列化则只需要传入对象的类型，而不需要传入对象的每一个属性的类型信息
            Kryo kryo = kryoThreadLocal.get();
            Object o = kryo.readObject(input, clazz);
            kryoThreadLocal.remove();
            return o;
        }
    }

    @Override
    public int getCode() {
        return SerializerCode.valueOf("KRYO").getCode();
    }
}
```

根据官网的教程，因为kryo不是线程安全的，因此使用的时候可以用ThreadLocal或者池化技术来创建Kryo，序列化时，先创建一个OutPut对象（是Kryo中的概念），然后使用writeObject方法吧object写入到output中，在调用output的getByte方法就可以获得到对应的字节数组。而反序列化就是从input对象中读调用readObject方法，只需要传入对象的类型，不需要传入每一个属性的类型信息。

## 第五章

基于Nacos的服务注册于发现

注册中心的作用是服务端将本地提供的方法名称和地址注册进Nacos中，客户端根据自己调用的接口名字获取到远程服务端的地址，进而与该服务端建立通道，向服务端发送RpcRequest请求，服务端收到后解析，然后根据请求解析出来的接口名字获取到类的对象，然后执行对应的方法得到结果后包装成RPCResponse对象发送给客户端。

所以**LocalServiceProvider**作用是服务端本地保存的对外面提供的方法key为接口名字，value为对应的service实例。而LocalServiceProvider存在的意义是什么？和ServiceRegistry的区别是什么？

ServiceRegistry是注册中心，注册的是服务名称和提供服务的地址，获取到地址后与服务端建立通道就可以传输数据了，而ServiceProvider是服务提供者，提供的是服务的实例，通过服务的名字直接获得服务的实例，获得到实例后通过反射调用对应的方法获得结果。

