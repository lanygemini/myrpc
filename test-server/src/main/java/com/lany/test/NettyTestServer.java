package com.lany.test;

import com.lany.api.HelloService;
import com.lany.rpc.serializer.CommonSerializer;
import com.lany.rpc.transport.netty.server.NettyServer;
import com.lany.rpc.provider.DefaultLocalServiceProvider;
import com.lany.rpc.serializer.KryoSerializer;

/**
 * @author jinchen
 * @version 1.0
 * @date 2021/12/25 17:13
 */
public class NettyTestServer {
    public static void main(String[] args) {
        NettyServer nettyServer = new NettyServer("127.0.0.1", 9999, CommonSerializer.JSON_SERIALIZER);
        HelloServiceImpl helloService = new HelloServiceImpl();
        nettyServer.publishService(helloService, HelloService.class);
        nettyServer.start();
    }
}
