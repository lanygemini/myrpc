package com.lany.test;

import com.lany.api.HelloObject;
import com.lany.api.HelloService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author liuyanyan
 * @date 2021/12/21 14:21
 * 服务端实现这个接口，表示服务端提供了这个类的实现
 */
public class HelloServiceImpl implements HelloService {
    private static final Logger logger = LoggerFactory.getLogger(HelloServiceImpl.class);

    @Override
    public String hello(HelloObject helloObject) {
        logger.info("接收到：{}", helloObject.getMessage());
        return "这时调用的返回值，id=" + helloObject.getId();
    }
}
