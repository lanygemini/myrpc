package com.lany.test;

import com.lany.api.HelloService;
import com.lany.rpc.provider.DefaultLocalServiceProvider;
import com.lany.rpc.provider.LocalServiceProvider;
import com.lany.rpc.serializer.CommonSerializer;
import com.lany.rpc.serializer.KryoSerializer;
import com.lany.rpc.transport.socket.server.SocketServer;

/**
 * @author liuyanyan
 * @date 2021/12/21 16:04
 */
public class SocketTestServer {

    public static void main(String[] args) {
        HelloServiceImpl helloService = new HelloServiceImpl();
        SocketServer rpcServer = new SocketServer("127.0.0.1", 9999, CommonSerializer.JSON_SERIALIZER);
        rpcServer.publishService(helloService, HelloService.class);
        rpcServer.start();
    }
}
