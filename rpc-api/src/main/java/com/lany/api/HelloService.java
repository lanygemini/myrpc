package com.lany.api;

/**
 * @author liuyanyan
 * @date 2021/12/21 14:19
 * 声明一个可以被外界调用的接口
 */
public interface HelloService {
    String hello(HelloObject helloObject);
}
